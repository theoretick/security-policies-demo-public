# security-policies

# security policies demo

Demo project containing a simple security policy and custom ruleset configuration.

Note: This is a public fork of https://gitlab.com/theoretick/security-policies-demo-private

## Usage

See https://gitlab.com/theoretick/security-policies-demo-public for how this project is used